from math import *

def LineInterCircle(x1,y1,x2,y2,r,x3,y3):
     #1:  Equation of the line
    if x1==x2:
        x1*=1.000001 #just to avoid division by 0 LMAO , we round the values so the answer will be 100% accurate anyway lol
    slope= (y2-y1)/(x2-x1)
    b= y1-slope*x1
    # print(slope)
    # print(b)
    
    # I solved the equation of the line with the eqaution of the circle: replaced for y and solved for X , I then used the quadralic formula
    # I did the math on paper, I can talk more about it during the interview
    
    A= 1+slope**2
    B= 2*slope*b - 2*x3 - 2*y3*slope
    C= x3**2 + y3**2 + b**2 - 2*y3*b - r**2
 
    if B**2 - 4*A*C <0 :
        print("No Intersection(s)")
    if B**2 - 4*A*C ==0 :
        X= -B/2*A
        Y= X*slope + b
        print("One Intersection(s):","(",round(X, 2),",",round(Y, 2),")")
    if B**2 - 4*A*C >0 :
        X1= (-B+sqrt(B**2 - 4*A*C ))/(2*A)
        Y1= X1*slope + b
        X2= (-B-sqrt(B**2 - 4*A*C ))/(2*A)
        Y2= X2*slope + b
        print("Two Intersection(s):","(",round(X1, 2),",",round(Y1, 2),") AND" , "(",round(X2, 2),",",round(Y2, 2),")" )
    
        
LineInterCircle(4,6,4,11,5,3,9)
LineInterCircle(0,-10,15,15,5,9,3)
LineInterCircle(0,-10,15,15,10,-5,4)
LineInterCircle(0,10,30,10,10,12,0)
LineInterCircle(3,-10,15,4,4,9,4)
LineInterCircle(5,1,12,-3,15,-2,9)

